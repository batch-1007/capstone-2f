//Add the logic that will show the login button when a user is not logged in

/* not kapag nag sasabi siya ng null inner html paki check kung yung mga navitems, navprofile and registerBtn ay nasa pages na needed sila dahil sa condition natin na mag didisplay pag naka login*/
let navItems = document.querySelector("#navSession")
let navProfile = document.querySelector("#navProfile")
let navRegister = document.querySelector("#navRegister")
let registerBtn = document.querySelector("#registerBtn")
console.log(navItems)
console.log(registerBtn)


let userToken = localStorage.getItem("token")
console.log(userToken)



if(!userToken) {
	navItems.innerHTML = 
	`
		<li class="nav-item"> 
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
	`
	registerBtn.innerHTML =
	`
		<li class="nav-item"> 
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`

	/*to display the register*/
	// navRegister.style.display = "block"

}else{
	navItems.innerHTML = 
		`
		<li class="nav-item"> 
			<a href="./logout.html" class="nav-link"> Log out </a>
		</li>
		`
	navProfile.innerHTML =
		`
		<li class="nav-item"> 
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		`
		
}

