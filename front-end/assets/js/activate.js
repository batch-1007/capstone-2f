let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

fetch(`http://localhost:4000/api/courses/activate/${courseId}`,{
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})

.then(res => res.json())
.then(data =>{
	if(data === true){
		alert('Course Activated')
		window.location.replace("./courses.html")
	}else{
		alert("Something went wrong")
	}
})

