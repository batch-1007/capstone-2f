

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')


let editCourseForm = document.querySelector("#editCourse");

editCourseForm.addEventListener('submit', (e) =>{
	e.preventDefault();

	
	let courseName = document.querySelector("#courseName").value;
	let price = document.querySelector("#coursePrice").value;
	let description = document.querySelector("#courseDescription").value

	
	fetch('http://localhost:4000/api/courses', {

		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		},
			body: JSON.stringify({
			courseId:`${courseId}`,	
			name: courseName,
			description : description ,
			price: price
		})
	})

	.then(res => {
	return res.json()
	})
	.then(data => {
		//create of new course sucessful
		if(data === true){
			//redirect to course page
			alert("")
			window.location.replace("./courses.html")
		}else {
			// redirect in creating courses
			alert("Something went wrong.")
		}

	})
 })

